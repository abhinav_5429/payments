from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import NotFound

from orders.models import Transaction
from orders.serializers import TransactionSerializer

class TransactionDetails(APIView):

    def get(self, request, transaction_id, *args, **kwargs):
        try:
            transaction = Transaction.objects.get(pk=transaction_id)
        except Transaction.DoesNotExist:
            raise NotFound(f'Transaction with id {transaction_id} does not exist')

        transaction_serializer = TransactionSerializer(transaction)

        return Response({
            'transaction': transaction_serializer.data,
            })