from django.db import transaction
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework import permissions

from orders.models import Package, Order, Transaction
import constants
import razorpay


class CreateTransaction(APIView):
    permissions = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        payment_method = request.data.get('payment_method')
        try:
            with transaction.atomic():
                packages = self.get_packages()
                orders = self.get_orders(packages)
                payment_transaction = Transaction(user_id=1)
                payment_transaction.total_amount = sum(order.amount for order in orders)
                payment_transaction.payment_method = payment_method
                gateway = self.get_gateway_details(payment_method)
                payment_transaction.gateway_name = gateway
                payment_transaction.save()
                payment_transaction.orders.add(*orders)
                return Response({
                        'transaction_id': payment_transaction.id,
                        'gateway': gateway,
                        'gateway_payload': payment_transaction.get_payload(),
                        'transaction_url': self.get_transaction_url(gateway)
                    })
        except Exception as e:
            raise APIException(e)

    @staticmethod
    def get_packages():
        return Package.objects.filter(status=constants.Miscallaneous.STATUS_ACTIVE)

    @staticmethod
    def get_orders(packages):
        orders = []
        for package in packages:
            try:
                order = Order.objects.get(package=package, user_id=1)
            except Order.DoesNotExist:
                order = Order.objects.create(package=package, user_id=1, amount=package.total_amount)
            orders.append(order)
        return orders

    def get_gateway_details(self, payment_method):
        if payment_method in (constants.PaymentMethods.PAYTM_WALLET, constants.PaymentMethods.PAYTM_PAYMENTS_BANK,
                              constants.PaymentMethods.UPI):
            gateway = constants.Gateways.PAYTM
        elif payment_method in (constants.PaymentMethods.CREDIT_CARD, constants.PaymentMethods.DEBIT_CARD,
                                constants.PaymentMethods.NETBANKING):
            gateway = constants.Gateways.RAZORPAY
        else:
            raise APIException(f'Invalid payment method {payment_method}')
        return gateway

    def get_transaction_url(self, gateway):
        if gateway == constants.Gateways.PAYTM:
            return settings.PAYTM_SETTINGS['TRANSACTION_URL']
        elif gateway ==constants.Gateways.RAZORPAY:
            return settings.RAZORPAY_SETTINGS['TRANSACTION_URL']
        else:
            raise APIException(f'Invalid Gateway {gateway}')