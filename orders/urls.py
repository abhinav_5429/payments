from django.urls import re_path, path, include
from orders.api_views.create_transaction import CreateTransaction
from orders.api_views.transaction_details import TransactionDetails
from orders.api_views.process_transaction import ProcessTransaction

urlpatterns = [
    re_path(r'create-transaction/$', CreateTransaction.as_view()),
    re_path(r'transaction-details/(?P<transaction_id>\d+)', TransactionDetails.as_view()),
    re_path(r'process-transaction/$', ProcessTransaction.as_view())
]
