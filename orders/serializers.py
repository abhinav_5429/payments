from django.conf import settings
from rest_framework import serializers

from orders.models import Transaction, Order, Package
import constants


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'

class TransactionSerializer(serializers.ModelSerializer):
    orders = OrderSerializer(read_only=True, many=True)
    gateway_payload = serializers.SerializerMethodField()
    transaction_url = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = '__all__'

    def get_gateway_payload(self, instance):
        return instance.get_payload()

    def get_transaction_url(self, instance):
        if instance.gateway_name == constants.Gateways.PAYTM:
            return settings.PAYTM_SETTINGS['TRANSACTION_URL']
        elif instance.gateway_name == constants.Gateways.RAZORPAY:
            return settings.RAZORPAY_SETTINGS['TRANSACTION_URL']
        else:
            raise APIException(f'Invalid gateway {instance.gateway_name}')