class Test:
    STATUS_OPEN = 'open'
    STATUS_CLOSED = 'closed'
    STATUS_DRAFT = 'draft'
    STATUS_LIVE = 'live'
    LINKING_SINGLE = 'single'
    LINKING_PARENT = 'parent'
    LINKING_CHILD = 'child'


class TestSeries:
    TYPE_PREVIOUS_PAPER = 'previous-paper'
    TYPE_TEST_SERIES = 'test-series'
    TYPE_MODEL_TEST_PAPER = 'model-paper'
    TYPE_QUESTION_BANK = 'question-bank'
    STATUS_CLOSED = 'closed'
    STATUS_DRAFT = 'draft'
    STATUS_LIVE = 'live'
