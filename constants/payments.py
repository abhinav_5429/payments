class Gateways:
    RAZORPAY = 'razorpay'
    PAYTM = 'paytm'
    CASH = 'cash'
    ACCOUNT_TRANSFER = 'account-transfer'

class PaymentMethods:
    CREDIT_CARD = 'credit-card'
    DEBIT_CARD = 'debit-card'
    PAYTM_WALLET = 'paytm-wallet'
    PAYTM_PAYMENTS_BANK = 'paytm-payments-bank'
    NETBANKING = 'netbanking'
    UPI = 'upi'
